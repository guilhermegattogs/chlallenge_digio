//
//  APIRequest.swift
//  challenge_cbss
//
//  Created by Guilherme Gatto on 26/11/18.
//  Copyright © 2018 Guilherme Gatto. All rights reserved.
//

import Foundation
import UIKit

class APIRequest{
    
    //MARK: - Attributes
    
    static private let session = URLSession(configuration: .default)
    
    //MARK: - Typealias
    
    typealias completionHandler = (ResponseEnum<DataObject>) -> Void
    typealias imageCompletionHandler = (ResponseEnum<UIImage>) -> Void
    
    
    //MARK: - Public Functions
    
    static func getData(completion: @escaping (completionHandler)){
        
        let endpoint = self.endPoint(url: APISupport.baseURL)
        
        let task = session.dataTask(with: URLRequest(url: endpoint)){ data, _ , error in
            
            let decoder = JSONDecoder()
            
            do{
                let response = try decoder.decode(Response.self, from: data!)
                completion(ResponseEnum.success(response))
            }catch{
                print(error)
            }
            
        }
        task.resume()
    }
    
    static func downloadImage(url: String, completion: @escaping (imageCompletionHandler)){
        
        let endpoint = self.endPoint(url: url)
        
        let task = session.dataTask(with: URLRequest(url: endpoint)) { (data, _, error) in
            
            if error != nil {
                completion(ResponseEnum.error(error!.localizedDescription))
            }else{
                let image = UIImage(data: data!)
                
                if let image = image {
                    completion(ResponseEnum.success(image))
                }else{
                    completion(ResponseEnum.success(UIImage(named: "notfound")!))
                }
            }
        }
        task.resume()
    }
    
    
    
    
    //MARK: - Private Functions
    
    static private func endPoint(url: String) -> URL{
        guard let baseUrl = URL(string: url) else {
            return URL(fileURLWithPath: "")
        }
        return baseUrl
    }
    
}
