//
//  Response.swift
//  challenge_cbss
//
//  Created by Guilherme Gatto on 26/11/18.
//  Copyright © 2018 Guilherme Gatto. All rights reserved.
//

import Foundation

enum ResponseEnum<T> {
    case success(T)
    case error(String)
}
