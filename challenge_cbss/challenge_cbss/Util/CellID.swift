//
//  CellID.swift
//  challenge_cbss
//
//  Created by Guilherme Gatto on 26/11/18.
//  Copyright © 2018 Guilherme Gatto. All rights reserved.
//

import Foundation

struct CellID {
    static let imageSpotlightCell = "imageSpotlightCell"
    static let imageTableCell = "imageTableCell"
    static let imageProductsCell = "ImageProductsCell"
    static let imageCashnCell = "imageCashnCell"
}
