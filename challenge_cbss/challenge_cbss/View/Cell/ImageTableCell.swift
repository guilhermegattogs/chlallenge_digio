//
//  ImageTableCell.swift
//  challenge_cbss
//
//  Created by Guilherme Gatto on 26/11/18.
//  Copyright © 2018 Guilherme Gatto. All rights reserved.
//

import UIKit

class ImageTableCell: UITableViewCell {

    @IBOutlet weak var oCollectionView: UICollectionView!
    
    var apiResponse: Response?
    var numberOfItensInSection = 0
    var tableSection = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.oCollectionView.delegate = self
        self.oCollectionView.dataSource = self
    }

    //MARK: Instantiate Reusable Cell
    func get(ofTableView tableView:UITableView, apiResponse: Response, for indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellID.imageTableCell, for: indexPath) as? ImageTableCell else {
            print("Failed to get ImageTableCell reusable cell")
            return UITableViewCell()
        }
        cell.apiResponse = apiResponse
        
        switch indexPath.section {
        case 0:
            cell.numberOfItensInSection = apiResponse.spotlight.count
            cell.tableSection = 0
        case 1:
            cell.numberOfItensInSection = 1
            cell.tableSection = 1
        case 2:
            cell.numberOfItensInSection = apiResponse.products.count
            cell.tableSection = 2
        default:
            break
        }
        return cell
    }

}

extension ImageTableCell: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItensInSection
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let apiResponse = apiResponse else {return UICollectionViewCell()}
        let imageCell = ImageSpotlightCell()
        let imageProductsCell = ImageProductsCell()
        let imageCashCell = ImageCashCellCell()
        
        switch tableSection {
        case 0:
            return imageCell.get(ofCollectionView: oCollectionView, imagerURL: apiResponse.spotlight[indexPath.row].bannerURL , for: indexPath)
        case 1:
            return imageCashCell.get(ofCollectionView: oCollectionView, imagerURL: apiResponse.cash.bannerURL , for: indexPath)
        case 2:
            return imageProductsCell.get(ofCollectionView: oCollectionView, imagerURL: apiResponse.products[indexPath.row].imageURL , for: indexPath)
        default:
            return UICollectionViewCell()
        }
    }
    
}

extension ImageTableCell: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = UIScreen.main.bounds.width
        let cellHeight = UIScreen.main.bounds.height
        
        switch tableSection {
        case 0:
            return CGSize(width:  cellWidth * 0.9, height: cellHeight * 0.3)
        case 1:
            return CGSize(width:  cellWidth * 0.9, height: cellHeight * 0.25)
        case 2:
            return CGSize(width:  cellHeight * 0.2, height: cellHeight * 0.2)
        default:
            return .zero
        }
    }
    
    
}
