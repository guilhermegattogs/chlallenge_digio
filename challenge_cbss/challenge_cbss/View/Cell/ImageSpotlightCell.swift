//
//  ImageCollectionCell.swift
//  challenge_cbss
//
//  Created by Guilherme Gatto on 26/11/18.
//  Copyright © 2018 Guilherme Gatto. All rights reserved.
//

import UIKit

class ImageSpotlightCell: UICollectionViewCell {
    
    @IBOutlet weak var oBackgroundView: UIView!
    @IBOutlet weak var oImageView: UIImageView!
    @IBOutlet weak var oActivityIndicator: UIActivityIndicatorView!
    
    var shadowView: UIView?
    override func awakeFromNib() {
     	super.awakeFromNib()
        createShadowView()
        roundCorners()
    }
    
    
    func get(ofCollectionView collectionView:UICollectionView, imagerURL: String, for indexPath:IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID.imageSpotlightCell, for: indexPath) as? ImageSpotlightCell else {return UICollectionViewCell()}
        
        cell.oImageView.contentMode = .scaleToFill
        cell.oActivityIndicator.isHidden = false
        cell.oActivityIndicator.startAnimating()
  
        APIRequest.downloadImage(url: imagerURL) { (response) in
            switch response{
            case .success(let image):
                DispatchQueue.main.async {
                    cell.oImageView.image = image
                    cell.oActivityIndicator.stopAnimating()
                    cell.oActivityIndicator.isHidden = true
                }
            case .error(_):
                break
            }
        }
       return cell
    }
    
}

extension ImageSpotlightCell{
    
    private func roundCorners() {
        self.oBackgroundView.layer.masksToBounds = true
        self.oBackgroundView.layer.cornerRadius = 8
    }
    
    private func createShadowView() {
        
        self.shadowView?.removeFromSuperview()
        let shadowView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.9, height: UIScreen.main.bounds.height * 0.3))
        self.insertSubview(shadowView, at: 0)
        self.shadowView = shadowView
        self.shadowView?.layer.applySketchShadow()
    }
    
}
