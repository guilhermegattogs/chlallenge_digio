//
//  HomeController.swift
//  challenge_cbss
//
//  Created by Guilherme Gatto on 26/11/18.
//  Copyright © 2018 Guilherme Gatto. All rights reserved.
//

import UIKit

class HomeController: UIViewController {


    //MARK: - Outlets
    
    @IBOutlet weak var oTableView: UITableView!
    
    
    //MARK: - Attributes
    
    var apiResponse: Response?
    
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        oTableView.delegate = self
        oTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        APIRequest.getData { (response) in
            switch response {
            case .success(let result as Response):
                self.apiResponse = result
                DispatchQueue.main.async {
                    self.oTableView.reloadData()
                }
            case .success(_):
                break
            case .error(let error):
                print(error)
            }
        }
        
    }
}

extension HomeController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let apiResponse = apiResponse else {return UITableViewCell()}
        let imageCell = ImageTableCell()
        return imageCell.get(ofTableView: oTableView, apiResponse: apiResponse, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 200
        case 1:
            return 150
        case 2:
            return 150
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return "Digio cash"
        case 2:
            return "Products"
        default:
            return ""
        }
    }
}

