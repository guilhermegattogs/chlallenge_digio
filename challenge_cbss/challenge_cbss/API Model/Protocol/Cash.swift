//
//  Cash.swift
//  challenge_cbss
//
//  Created by Guilherme Gatto on 26/11/18.
//  Copyright © 2018 Guilherme Gatto. All rights reserved.
//

import Foundation

struct Cash: DataObject {
    var title: String
    var bannerURL: String
}
